import logo from './logo.svg';
import './App.css';
import { connect } from 'react-redux';
import { useEffect, useState } from 'react';
import { latestVersion, login, verfiOtp } from './services/user';
import actions from './redux/user/actions';

function App(props) {
  const [name, setName] = useState('Manish');
  const [number, setNumber] = useState('');
  const [otp, setOtp] = useState('');
  const [isOtpFormVisible, setOtpFormVisible] = useState(false);

  const { dispatch, user } = props;

  useEffect(() => {
    dispatch({
      type: actions.SET_STATE,
      payload: {
        loading: true,
      },
    });
    latestVersion().then((res) => {
      dispatch({
        type: actions.SET_STATE,
        payload: res.data,
      });
      dispatch({
        type: actions.SET_STATE,
        payload: {
          loading: false,
        },
      });
      return res;
    });
  }, []);

  const handleLogin = () => {
    dispatch({
      type: actions.SET_STATE,
      payload: {
        loading: true,
      },
    });

    const loginPayload = {
      country_code: '91',
      mobile: `91${number}`,
    };

    login(loginPayload).then((res) => {
      console.log(res);

      if (res && res.data.code === 201) {
        setOtpFormVisible(true);
      }

      dispatch({
        type: actions.SET_STATE,
        payload: {
          loading: false,
        },
      });
    });
  };

  const handleOtp = () => {
    dispatch({
      type: actions.SET_STATE,
      payload: { loading: true },
    });

    const otpPayload = {
      country_code: '91',
      mobile: `91${number}`,
      code: otp,
      device_token: null,
    };

    verfiOtp(otpPayload).then((res) => {
      console.log(res);

      if (res && res.data.code === 200) {
        const token = res.data.data.token;
        dispatch({
          type: actions.SET_STATE,
          payload: { token },
        });
      }
    });

    dispatch({
      type: actions.SET_STATE,
      payload: { loading: false },
    });
  };

  if (user.loading) {
    return (
      <div className='App'>
        <header className='App-header'>
          <img src={logo} className='App-logo' alt='logo' />
          <p>Loading</p>
        </header>
      </div>
    );
  }

  return (
    <div className='App'>
      <header className='App-header'>
        <img src={logo} className='App-logo' alt='logo' />
        <p>{name}</p>
        <p>
          {user.token ? 'User has been logged in' : 'User not logged in yet'}
        </p>

        {!isOtpFormVisible && (
          <div className='login-form'>
            <input
              value={number}
              onChange={(e) => setNumber(e.target.value)}
              placeholder='Enter Number'
            />
            <button onClick={handleLogin}>Login</button>
          </div>
        )}

        {isOtpFormVisible && (
          <div className='login-form'>
            <input
              value={otp}
              onChange={(e) => setOtp(e.target.value)}
              placeholder='Enter OTP'
            />
            <button onClick={handleOtp}>Verify OTP</button>
          </div>
        )}
      </header>
    </div>
  );
}

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps)(App);
