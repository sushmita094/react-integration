const actions = {
  LOGIN: 'user/LOGIN',
  LOGIN_ASYNC: 'user/LOGIN_ASYNC',
  LOGOUT: 'user/LOGOUT',
  SET_STATE: 'user/SET_STATE',
};

export default actions;
