import axiosInstance from '../utilities/configureAxios';

export async function login(payload) {
  return axiosInstance
    .post('/login', payload)
    .then((res) => res)
    .catch((error) => {
      console.log(error);
    });
}

export async function verfiOtp(payload) {
  return axiosInstance
    .post('/verify-otp', payload)
    .then((res) => res)
    .catch((error) => {
      console.log(error);
    });
}

export async function latestVersion() {
  return axiosInstance
    .get('/latest-version')
    .then((res) => {
      return res;
    })
    .catch((error) => {
      console.log(error);
    });
}
